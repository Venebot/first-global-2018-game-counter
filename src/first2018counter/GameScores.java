/*
 * This code is property of Venebot (Team Venezuela). It is free to use for 
 * competitive, educative and non-economic purposes, as long as proper acreditation 
 * is given to the team, and no harmful actions are done with its use.
 */
package first2018counter;

/**
 *
 * @author José Ramírez <josedramirez07@gmail.com> from Venebot (Team Venezuela)
 * <robotsvenezuela@gmail.com>
 */
public class GameScores {

    public static final int SCORE_PER_SOLAR_PANEL = 1;
    public static final int SCORE_EOLIC_POWER = 1;
    public static final int SCORE_REACTIVE_PLANT = 3;
    public static final int SCORE_PER_LOWER_COMBUSTION = 5;
    public static final int SCORE_PER_HIGHER_COMBUSTION = 20;
    public static final int SCORE_BONUS_COPERTITION = 100;
    public static final int SCORE_PARKING = 15;
    public static final int SCORE_BONUS_PARKING = 5;
}
