/*
 * This code is property of Venebot (Team Venezuela). It is free to use for 
 * competitive, educative and non-economic purposes, as long as proper acreditation 
 * is given to the team, and no harmful actions are done with its use.
 */
package first2018counter;

import static first2018counter.GameScores.SCORE_EOLIC_POWER;
import static first2018counter.GameScores.SCORE_REACTIVE_PLANT;

/**
 *
 * @author José Ramírez <josedramirez07@gmail.com> from Venebot (Team Venezuela) <robotsvenezuela@gmail.com>
 */
public class Game {

    public static final int PLAYERS = 3;
    public static final int MAX_SOLAR_PANELS = 3;
    public static final int GAME_SECS = (2 * 60) + 30; //2 minutes 30 seconds
    public static final int REACTIVE_PLANT_SLOTS = 8;

    private boolean isCombustionPlantConnected;
    private int lowerCombustionCubes;
    private int higherCombustionCubes;

    private int installedSolarPanels;
    private int solarPanelScore;

    private boolean isReactivePlantConnected;
    private int reactivePlantUsedSlots;
    private boolean isReactivePlantActive;
    private int reactivePlantScore;

    private boolean isEolicPlantConnected;
    private boolean isEolicPlantActive;
    private int eolicPlantScore;

    private int parkedRobots;

    private boolean isCoopertitionConnected;
    private boolean hasCoopertition;

    public void updateScore() {
        solarPanelScore += installedSolarPanels * GameScores.SCORE_PER_SOLAR_PANEL;
        eolicPlantScore += isEolicPlantActive ? SCORE_EOLIC_POWER : 0;
        reactivePlantScore += isReactivePlantActive ? SCORE_REACTIVE_PLANT : 0;

        //TODO - BUT PROBABLY NEVER WILL DO - : ADD SUPPORT FOR TWO TEAMS AND COOPERTITION
        //if (!hasCoopertition && isCoopertitionConnected) {
        //if (otherTeam.isCoopertitionConnected) {
        //hasCoopertition = true
        //score += GameCounter.SCORE_BONUS_COPERTITION;
        //}
    }

    public void connectReactivePlant() {
        if (!isReactivePlantConnected) {
            isReactivePlantConnected = true;
            if (getReactivePlantUsedSlots() == REACTIVE_PLANT_SLOTS) {
                isReactivePlantActive = true;
            }
        }
    }

    public void connectEolicPlant() {
        if (!isEolicPlantConnected) {
            isEolicPlantConnected = true;
        }
    }

    public void fillReactivePlantSlot() {
        if (getReactivePlantUsedSlots() < REACTIVE_PLANT_SLOTS) {
            reactivePlantUsedSlots++;
            if (getReactivePlantUsedSlots() == REACTIVE_PLANT_SLOTS && isReactivePlantConnected) {
                isReactivePlantActive = true;
            }
        }
    }

    public void activateEolicPlant() {
        if (isReactivePlantConnected) {
            isEolicPlantActive = true;
        }
    }

    public void addSolarPanel() {
        if (installedSolarPanels < MAX_SOLAR_PANELS) {
            installedSolarPanels++;
        }
    }

    public void parkRobot() {
        if (parkedRobots < PLAYERS) {
            parkedRobots++;
        }
    }

    public void connectCoopertition() {
        isCoopertitionConnected = true;
        hasCoopertition = true; //TODO: REMOVE THIS IF I EVER ADD SUPPORT FOR TWO TEAMS
    }

    public void connectCombustionPlant() {
        isCombustionPlantConnected = true;
    }

    public void scoreHigherCombustion() {
        higherCombustionCubes++;
    }

    public void scoreLowerCombustion() {
        lowerCombustionCubes++;
    }

    /**
     * @return the isCombustionPlantConnected
     */
    public boolean isCombustionPlantConnected() {
        return isCombustionPlantConnected;
    }

    /**
     * @return the lowerCombustionCubes
     */
    public int getLowerCombustionCubes() {
        return lowerCombustionCubes;
    }

    /**
     * @return the higherCombustionCubes
     */
    public int getHigherCombustionCubes() {
        return higherCombustionCubes;
    }

    /**
     * @return the installedSolarPanels
     */
    public int getInstalledSolarPanels() {
        return installedSolarPanels;
    }

    /**
     * @return the isReactivePlantConnected
     */
    public boolean isReactivePlantConnected() {
        return isReactivePlantConnected;
    }

    /**
     * @return the isReactivePlantActive
     */
    public boolean isReactivePlantActive() {
        return isReactivePlantActive;
    }

    /**
     * @return the isEolicPlantConnected
     */
    public boolean isEolicPlantConnected() {
        return isEolicPlantConnected;
    }

    /**
     * @return the isEolicPlantActive
     */
    public boolean isEolicPlantActive() {
        return isEolicPlantActive;
    }

    /**
     * @return the parkedRobots
     */
    public int getParkedRobots() {
        return parkedRobots;
    }

    /**
     * @return the hasCoopertition
     */
    public boolean hasCoopertition() {
        return hasCoopertition;
    }

    /**
     * @return the isCoopertitionConnected
     */
    public boolean isCoopertitionConnected() {
        return isCoopertitionConnected;
    }

    /**
     * @return the solarPanelScore
     */
    public int getSolarPanelScore() {
        return solarPanelScore;
    }

    /**
     * @return the reactivePlantScore
     */
    public int getReactivePlantScore() {
        return reactivePlantScore;
    }

    /**
     * @return the eolicPlantScore
     */
    public int getEolicPlantScore() {
        return eolicPlantScore;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return getSolarPanelScore()
                + getReactivePlantScore()
                + getEolicPlantScore()
                + (isCombustionPlantConnected ? 
                    (lowerCombustionCubes * GameScores.SCORE_PER_LOWER_COMBUSTION) + (higherCombustionCubes * GameScores.SCORE_PER_HIGHER_COMBUSTION) : 0)
                + (parkedRobots * GameScores.SCORE_PARKING)
                + (parkedRobots == PLAYERS ? GameScores.SCORE_BONUS_PARKING : 0)
                + (hasCoopertition ? GameScores.SCORE_BONUS_COPERTITION : 0);
    }

    /**
     * @return the reactivePlantUsedSlots
     */
    public int getReactivePlantUsedSlots() {
        return reactivePlantUsedSlots;
    }
}
