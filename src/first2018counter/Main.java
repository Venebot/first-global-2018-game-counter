/*
 * This code is property of Venebot (Team Venezuela). It is free to use for 
 * competitive, educative and non-economic purposes, as long as proper acreditation 
 * is given to the team, and no harmful actions are done with its use.
 */
package first2018counter;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author José Ramírez <josedramirez07@gmail.com> from Venebot (Team Venezuela) <robotsvenezuela@gmail.com>
 */
public class Main extends Application {

    private Game game;
    private Timeline gameTimer;
    private SimpleIntegerProperty currentTime;

    private Button btnStartGame;
    private Label lblTime;
    private Label lblScore;

    private Button btnConnectCombustionPlant;
    private Button btnScoreLowerCombustion;
    private Button btnScoreHigherCombustion;

    private Button btnInstallSolarPanel;

    private Button btnConnectReactivePlant;
    private Button btnFillReactivePlantSlot;

    private Button btnConnectEolicPlant;
    private Button btnActivateEolicPlant;

    private Button btnConnectCoorpetition;

    private Button btnParkBot;

    @Override
    public void start(Stage primaryStage) {
        setupTimer();
        setupTimerButton();
        setupControlButtons();

        lblTime = new Label();
        lblTime.textProperty().bind(Bindings.concat("Time: ", currentTime));
        
        lblScore = new Label("Score: 0");

        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);
        root.getChildren().addAll(lblTime, btnStartGame);
        root.getChildren().addAll(btnConnectCombustionPlant,
                btnScoreLowerCombustion,
                btnScoreHigherCombustion,
                btnInstallSolarPanel,
                btnConnectReactivePlant,
                btnFillReactivePlantSlot,
                btnConnectEolicPlant,
                btnActivateEolicPlant,
                btnConnectCoorpetition,
                btnParkBot);
        
        root.getChildren().addAll(lblScore);

        Scene scene = new Scene(root, 500, 500);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setupTimerButton() {
        btnStartGame = new Button();
        btnStartGame.setOnAction((ActionEvent event) -> onActionBtnStartGame());
        btnStartGame.textProperty().bind(Bindings.when(gameTimer.statusProperty().isEqualTo(Animation.Status.RUNNING)).then("STOP").otherwise("RUN"));
    }

    private void setupTimer() {
        currentTime = new SimpleIntegerProperty(0);
        gameTimer = new Timeline();
        gameTimer.setCycleCount(Timeline.INDEFINITE);
        gameTimer.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1), (ActionEvent event) -> {
                    if (currentTime.get() != 0) {
                        currentTime.set(currentTime.get() - 1);
                        game.updateScore();
                        lblScore.setText("Score: " + game.getScore());
                    } else {
                        gameTimer.stop();
                    }
                }));
    }

    private void onActionBtnStartGame() {
        if (gameTimer.statusProperty().get() != Animation.Status.RUNNING) {
            currentTime.set(Game.GAME_SECS);
            game = new Game();
            resetControlButtons();
            gameTimer.playFromStart();
            lblScore.setText("Score: 0");
        } else {
            disableControlButtons();
            gameTimer.stop();
        }
    }

    private void setupControlButtons() {
        btnConnectCombustionPlant = new Button("Connect Combustion Plant Power");
        btnScoreLowerCombustion = new Button("Score Lower Combustion");
        btnScoreHigherCombustion = new Button("Score Higher Combustion");
        btnInstallSolarPanel = new Button("Install Solar Panel");
        btnConnectReactivePlant = new Button("Connect Reactive Plant Power");
        btnFillReactivePlantSlot = new Button("Fill Reactive Plant Slot");
        btnConnectEolicPlant = new Button("Connect Eolic Plant Power");
        btnActivateEolicPlant = new Button("Activate Eolic Plant Handle");
        btnConnectCoorpetition = new Button("Connect Coompertition Power");
        btnParkBot = new Button("Park Robot");

        btnConnectCombustionPlant.setOnAction((event) -> {
            game.connectCombustionPlant();
            btnConnectCombustionPlant.setDisable(true);
        });
        btnScoreLowerCombustion.setOnAction((event) -> game.scoreLowerCombustion());
        btnScoreHigherCombustion.setOnAction((event) -> game.scoreHigherCombustion());
        btnInstallSolarPanel.setOnAction((event) -> {
            game.addSolarPanel();
            if (game.getInstalledSolarPanels() == Game.MAX_SOLAR_PANELS) {
                btnInstallSolarPanel.setDisable(true);
            }
        });
        btnConnectReactivePlant.setOnAction((event) -> {
            game.connectReactivePlant();
            btnConnectReactivePlant.setDisable(true);
        });
        btnFillReactivePlantSlot.setOnAction((event) -> {
            game.fillReactivePlantSlot();
            if (game.getReactivePlantUsedSlots() == Game.REACTIVE_PLANT_SLOTS) {
                btnFillReactivePlantSlot.setDisable(true);
            }
        });
        btnConnectEolicPlant.setOnAction((event) -> {
            game.connectEolicPlant();
            btnConnectEolicPlant.setDisable(true);
            btnActivateEolicPlant.setDisable(false);
        });
        btnActivateEolicPlant.setOnAction((event) -> {
            game.activateEolicPlant();
            btnActivateEolicPlant.setDisable(true);
        });
        btnConnectCoorpetition.setOnAction((event) -> {
            game.connectCoopertition();
            btnConnectCoorpetition.setDisable(true);
        });
        btnParkBot.setOnAction((event) -> {
            game.parkRobot();
            if (game.getParkedRobots() == Game.PLAYERS) {
                btnParkBot.setDisable(true);
                disableControlButtons();
            }
        });
        
        disableControlButtons();
    }

    private void resetControlButtons() {
        btnConnectCombustionPlant.setDisable(false);
        btnScoreLowerCombustion.setDisable(false);
        btnScoreHigherCombustion.setDisable(false);
        btnInstallSolarPanel.setDisable(false);
        btnConnectReactivePlant.setDisable(false);
        btnFillReactivePlantSlot.setDisable(false);
        btnConnectEolicPlant.setDisable(false);
        btnActivateEolicPlant.setDisable(true);
        btnConnectCoorpetition.setDisable(false);
        btnParkBot.setDisable(false);
    }

    private void disableControlButtons() {
        btnConnectCombustionPlant.setDisable(true);
        btnScoreLowerCombustion.setDisable(true);
        btnScoreHigherCombustion.setDisable(true);
        btnInstallSolarPanel.setDisable(true);
        btnConnectReactivePlant.setDisable(true);
        btnFillReactivePlantSlot.setDisable(true);
        btnConnectEolicPlant.setDisable(true);
        btnActivateEolicPlant.setDisable(true);
        btnConnectCoorpetition.setDisable(true);
        btnParkBot.setDisable(true);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
